#!/bin/sh

# Runs WP-CLI commands as www-data
# to avoid permissions issues

su-exec www-data wp-cli "$@"