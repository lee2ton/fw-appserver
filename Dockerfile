#PHP Build inspired by wordpress:php-fpm-alpine/and Pantheon,Kalabox

FROM php:7.2.1-fpm-alpine3.7
LABEL Maintainer="Lee Pang <lee@2ton.com>" \
      Description="Lightweight PHP-FPM 7.2 container based on Alpine Linux."

WORKDIR /var/www

COPY wp-fix.sh /usr/local/bin/wp

RUN set -ex; \
    \
    apk add --no-cache --virtual .build-deps \
        libjpeg-turbo-dev \
        libpng-dev \
        libxml2-dev \
        libmcrypt-dev \
        autoconf \
        zlib-dev \
        g++ \
        make; \
    pecl install redis-3.1.2; \
    pecl install mcrypt-1.0.1; \
    docker-php-ext-configure gd --with-png-dir=/usr --with-jpeg-dir=/usr; \
    docker-php-ext-install gd mysqli opcache soap; \
    docker-php-ext-enable mcrypt redis; \
    curl -o /usr/local/bin/wp-cli \
        https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar; \
    chmod +x /usr/local/bin/wp-cli /usr/local/bin/wp; \
    runDeps="$( \
        scanelf --needed --nobanner --format '%n#p' --recursive /usr/local/lib/php/extensions \
            | tr ',' '\n' \
            | sort -u \
            | awk 'system("[ -e /usr/local/lib/" $1 " ]") == 0 { next } { print "so:" $1 }' \
    )"; \
    apk add --virtual .wordpress-phpexts-rundeps $runDeps; \
    apk del .build-deps \
        autoconf \
        g++ \
        make; \
    apk add --no-cache shadow su-exec; \
    rm -rf /var/cache/apk/* /tmp/* /var/www/html